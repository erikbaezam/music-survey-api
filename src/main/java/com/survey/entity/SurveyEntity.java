package com.survey.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class SurveyEntity implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -9158908533641712129L;
	
	@Id
	@GeneratedValue
	private Integer id;
	private String email;
	private String genero;
}
