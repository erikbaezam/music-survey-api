package com.survey.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.survey.dto.SurveyDTO;
import com.survey.dto.SurveyResponseDTO;
import com.survey.service.SurveyService;

@RestController
public class SurveyController {

	@Autowired
	private SurveyService surveyService;

	@PostMapping(value = "/saveSurvey", produces = MediaType.APPLICATION_JSON_VALUE)
	public SurveyResponseDTO submitSurvey(@RequestBody SurveyDTO surveyDTO) {
		return surveyService.submitResponse(surveyDTO);
	}

	@GetMapping(value = "/getAllResults", produces = MediaType.APPLICATION_JSON_VALUE)
	public List<SurveyDTO> getAllResult() {
		return surveyService.getAllResult();

	}
}
