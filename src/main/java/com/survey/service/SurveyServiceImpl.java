package com.survey.service;

import java.util.List;
import java.util.stream.Collectors;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.survey.dto.SurveyDTO;
import com.survey.dto.SurveyResponseDTO;
import com.survey.entity.SurveyEntity;
import com.survey.repository.SurveyRepository;

@Service
public class SurveyServiceImpl implements SurveyService {

	@Autowired
	private SurveyRepository surveyRepository;

	private final DozerBeanMapper dozerBeanMapper = new DozerBeanMapper();

	@Override
	public SurveyResponseDTO submitResponse(SurveyDTO surveyDTO) {
		SurveyResponseDTO surveyResponseDTO = SurveyResponseDTO.builder().build();
		try {
			surveyRepository.save(dozerBeanMapper.map(surveyDTO, SurveyEntity.class));
			surveyResponseDTO.setResponse("Succes");

		} catch (Exception e) {
			surveyResponseDTO.setResponse("Failure " + e.getMessage());
		}
		return surveyResponseDTO;
	}

	@Override
	public List<SurveyDTO> getAllResult() {
		return surveyRepository.findAll().stream().map(s -> dozerBeanMapper.map(s, SurveyDTO.class))
				.collect(Collectors.toList());
	}

}
