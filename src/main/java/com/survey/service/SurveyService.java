package com.survey.service;

import java.util.List;

import com.survey.dto.SurveyDTO;
import com.survey.dto.SurveyResponseDTO;

public interface SurveyService {

	SurveyResponseDTO submitResponse(SurveyDTO surveyDTO);

	List<SurveyDTO> getAllResult();
}
