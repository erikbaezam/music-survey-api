package com.survey.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class SurveyDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2975895828986543809L;
	
	private String email;
	private String genero;
}
