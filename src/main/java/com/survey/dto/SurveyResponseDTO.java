package com.survey.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class SurveyResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6001307229212343366L;

	private String response;

}
