package com.survey.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.survey.entity.SurveyEntity;

public interface SurveyRepository extends JpaRepository<SurveyEntity, Integer> {

}
